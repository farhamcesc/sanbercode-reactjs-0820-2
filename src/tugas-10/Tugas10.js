import React from 'react';

class Nama extends React.Component {
  render() {
    return <td>{this.props.name}</td>;
  }
}

class Harga extends React.Component {
    render() {
      return <td>{this.props.harga}</td>;
    }
}

class Berat extends React.Component {
render() {
    return <td>{this.props.berat/1000} KG</td>;
}
}

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class Tugas10 extends React.Component {
  render() {
    return (
      <>
      <div id='header-buah'>
          <h1>Tabel Harga Buah</h1>
      </div>
      <table id="table-buah">
          <thead>
          <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
          </tr>
          </thead>
          <tbody>
        {dataHargaBuah.map(el=> {
          return (
            <tr>
                <Nama name={el.nama}/> 
                <Harga harga={el.harga}/> 
                <Berat berat={el.berat}/> 
            </tr>
          )
        })}
        </tbody>
      </table>
      </>
    )
  }
}

export default Tugas10